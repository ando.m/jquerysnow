(function ($) {
  Drupal.behaviors.snow = {
    attach: function (context, settings) {
      $settings = Drupal.settings.jquerysnow.data;
      $(document).snowfall($settings);
    }
  };
})(jQuery);
