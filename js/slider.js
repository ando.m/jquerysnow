(function ($) {
  Drupal.behaviors.snow = {
    attach: function (context, settings) {

      var $slider_wrappers = $('.jquerysnow-slider-wrapper');
      $slider_wrappers.each(function (i) {
        var $self = $(this);
        var $slider_wrapper = $('<div/>').addClass('jquerysnow-slider');
        var $input = $("input", $self);
        $self.append($slider_wrapper);
        var $slider = $('.jquerysnow-slider', $self);
        var default_value = $("input", $self).val() || 1;
       
        $slider.slider({
          min: 1,
          max: 10,
          value: default_value,
          slide: function (event, ui) {
            $("input", $self).val(ui.value);
          }
        });
      });




      if ($('#minSize').length) {


        var $minSize = $('#minSize');
        $minSize.slider({
          min: 0,
          max: 100,
          value: 10,
          slide: function (event, ui) {
            $("#edit-minsizefield").val(ui.value);
          }
        });
        var valueMin = $minSize.slider("value");
//        $("#edit-minsizefield").val(valueMin);
      }
      if ($('#maxSize').length) {
        var $maxSize = $('#maxSize');
        $maxSize.slider({
          min: 0,
          max: 100,
          value: 30,
          slide: function (event, ui) {
            $("#edit-maxsizefield").val(ui.value);
          }
        });
        var valueMax = $maxSize.slider("value");
//        $("#edit-maxsizefield").val(valueMax);
      }





    }
  };
})(jQuery);
